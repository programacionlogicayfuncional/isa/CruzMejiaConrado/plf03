(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [xs] (not xs))
        g (fn [x] (vector? x))
        z (comp f g)]
    (z [1 2 3 4 5 6])))

(defn función-comp-2
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [x] (first x))
        z (comp f g)]
    (z {:a 1 :b 2})))

(defn función-comp-3
  []
  (let [f (fn [xs] (not xs))
        g (fn [x] (drop 13 x))
        z (comp f g)]
    (z [0 1 0 2 0 3 0 4])))

(defn función-comp-4
  []
  (let [f (fn [xs] (double? xs))
        g (fn [xs] (not xs))
        z (comp f g)]
    (z 3.14)))

(defn función-comp-5
  []
  (let [f (fn [xs] (/ 3 xs 2))
        g (fn [x] (* 3 x 3))
        z (comp f g)]
    (z 12)))

(defn función-comp-6
  []
  (let [f (fn [xs] (concat "false" xs))
        g (fn [xs] (str xs))
        h (fn [x] (string? x))
        z (comp f g h)]
    (z "Conrado")))

(defn función-comp-7
  []
  (let [f (fn [xs] (boolean? xs))
        g (fn [x] (char? x))
        z (comp g f)]
    (z false)))

(defn función-comp-8
  []
  (let [f (fn [xs] (str xs))
        g (fn [x] (double x))
        z (comp f g)]
    (z 3.14)))

(defn función-comp-9
  []
  (let [f (fn [xs] (+ 13 xs))
        g (fn [x] (* 13 x))
        h (fn [x] (/ 6 x))
        z (comp f g h)]
    (z 13)))

(defn función-comp-10
  []
  (let [f (fn [xs] (not xs))
        g (fn [x] (every? even? x))
        z (comp f g)]
    (z [6 5 4 3 2])))

(defn función-comp-11
  []
  (let [f (fn [xs] (group-by count xs))
        g (fn [x] (list x))
        z (comp f g)]
    (z ["a" "as" "asd" "aa" "asdf" "qwer"])))

(defn función-comp-12
  []
  (let [f (fn [xs] (some? xs))
        g (fn [x] (map-indexed list x))
        z (comp f g)]
    (z [:a :b :c])))

(defn función-comp-13
  []
  (let [f (fn [xs] (str xs))
        g (fn [x] (indexed? x))
        z (comp f g)]
    (z {:a 1 :b 2})))

(defn función-comp-14
  []
  (let [f (fn [xs] (range xs))
        g (fn [x] (count x))
        z (comp f g)]
    (z {:2 1 :3 "brisa" :1 [1 2 3]})))

(defn función-comp-15
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (reverse xs))
        h (fn [x] (shuffle x))
        z (comp f g h)]
    (z '(1 2 3 4 5))))

(defn función-comp-16
  []
  (let [f (fn [xs] (not xs))
        g (fn [x] (zero? x))
        z (comp f g)]
    (z -13)))

(defn función-comp-17
  []
  (let [f (fn [xs] (list 6 6 xs))
        g (fn [x] (count x))
        z (comp f g)]
    (z "Conrado")))

(defn función-comp-18
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (filterv number? xs))
        h (fn [xs] (list xs))
        z (comp f g h)]
    (z [0 1 0 1 0 1])))

(defn función-comp-19
  []
  (let [f (fn [xs] (dec xs))
        g (fn [x] (/ 14 x))
        h (fn [x] (* 4 x))
        z (comp f g h)]
    (z 14)))

(defn función-comp-20
  []
  (let [f (fn [xs] (== xs 36))
        g (fn [x] (* x x x))
        h (fn [x] (+ x x))
        z (comp f g)]
    (z 13)))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

(defn función-complement-1
  []
  (let [f (fn [x] (some? x))
        z (complement f)]
    (z 3.14)))

(defn función-complement-2
  []
  (let [f (fn [xs] (empty? xs))
        z (complement f)]
    (z #{})))

(defn función-complement-3
  []
  (let [f (fn [x] (sequential? x))
        z (complement f)]
    (z [1 2 3 4 5 7 5])))

(defn función-complement-4
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z 1)))

(defn función-complement-5
  []
  (let [f (fn [xs] (map? xs))
        z (complement f)]
    (z #{1 2 3 4 5 6})))

(defn función-complement-6
  []
  (let [f (fn [xs x] (contains? xs x))
        z (complement f)]
    (z #{:a :b} :f)))

(defn función-complement-7
  []
  (let [f (fn [xs] (vector? xs))
        z (complement f)]
    (z [1 2 3 4 5 6])))

(defn función-complement-8
  []
  (let [f (fn [xs] (nil? xs))
        z (complement f)]
    (z #{})))

(defn función-complement-9
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 3.14)))

(defn función-complement-10
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z 1)))

(defn función-complement-11
  []
  (let [f (fn [x] (list? x))
        z (complement f)]
    (z '(1 2 3 4 5))))

(defn función-complement-12
  []
  (let [f (fn [xs] (distinct? xs))
        z (complement f)]
    (z [1 2 3 4 5 5])))

(defn función-complement-13
  []
  (let [f (fn [x] (sorted? x))
        z (complement f)]
    (z '(1 2 3 4 5))))

(defn función-complement-14
  []
  (let [f (fn [xs] (seq? xs))
        z (complement f)]
    (z [1])))

(defn función-complement-15
  []
  (let [f (fn [xs] (rational? xs))
        z (complement f)]
    (z 1.0))

(defn función-complement-16
  []
  (let [f (fn [x] (decimal? x))
        z (complement f)]
    (z 1.0)))

(defn función-complement-17
  []
  (let [f (fn [xs] (string? xs))
        z (complement f)]
    (z "hello")))

(defn función-complement-18
  []
  (let [f (fn [xs] (zero? xs))
        z (complement f)]
    (z 0)))

(defn función-complement-19
  []
  (let [f (fn [x] (pos-int? x))
        z (complement f)]
    (z 1)))

(defn función-complement-20
  []
  (let [f (fn [x] (nat-int? x))
        z (complement f)]
    (z -1)))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)


(defn función-constantly-1
  []
  (let [xs [1 3 0 1 0 1]
        z (constantly xs)]
    (z 13)))

(defn función-constantly-2
  []
  (let [xs #{:a 2 :b 3}
        z (constantly xs)]
    (z 13)))

(defn función-constantly-3
  []
  (let [xs [1 0 1 0 1]
        z (constantly xs)]
    (z true)))

(defn función-constantly-4
  []
  (let [xs #{:a 2 :b 3 :c 4}
        z (constantly xs)]
    (z [0 1 0 0 1])))


(defn función-constantly-5
  []
  (let [xs {:a 2 :b 3 :c 4 :d 5}
        z (constantly xs)]
    (z "Hello people")))


(defn función-constantly-6
  []
  (let [xs (list 0 1 1 0 1)
        z (constantly xs)]
    (z nil)))

(defn función-constantly-7
  []
  (let [x false
        z (constantly x)]
    (z [0 1 0 1])))

(defn función-constantly-8
  []
  (let [x "Hello"
        z (constantly x)]
    (z {:a 1 :b 0 :c 1 :d 0})))

(defn función-constantly-9
  []
  (let [xs (rand-int 100)
        z (constantly xs)]
    (z [:a :b :c])))

(defn función-constantly-10
  []
  (let [x (str 01001)
        z (constantly x)]
    (z [0 0 0 1])))

(defn función-constantly-11
  []
  (let [x 13
        z (constantly x)]
    (z "hello")))

(defn función-constantly-12
  []
  (let [x (boolean? 1)
        z (constantly x)]
    (z false)))

(defn función-constantly-13
  []
  (let [x [1 2 3 4]
        z (constantly x)]
    (z "")))

(defn función-constantly-14
  []
  (let [x (list 1 2 3)
        z (constantly x)]
    (z "Hello")))

(defn función-constantly-15
  []
  (let [f ((fn [x xs] (zipmap x xs)) [:a :b :c :d :e] [1 2 3 4 5])
        z (constantly f)]
    (z [1 2 3 4 5])))

(defn función-constantly-16
  []
  (let [f ((fn [xs] vals [xs]) {:a "foo", :b "bar"})
        z (constantly f)]
    (z {:a 2 :b 3})))

(defn función-constantly-17
  []
  (let [f ((fn [xs x] take xs x) 3 [1 2])
        z (constantly f)]
    (z 123)))

(defn función-constantly-18
  []
  (let [f ((fn [xs x] subs xs x) "Conradrc27" 1)
        z (constantly f)]
    (z "Hello People")))

(defn función-constantly-19
  []
  (let [f ((fn [xs] (rest xs)) ["a" "b" "c" "d" "e"])
        z (constantly f)]
    (z #{})))

(defn función-constantly-20
  []
  (let [f ((fn [xs] (range xs)) 13)
        z (constantly f)]
    (z 10)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [f (fn [x] (pos? (peek x)))
        z (every-pred f)]
    (z '(0 1 2 3 4 5 6 7 8 9))))

(defn función-every-pred-2
  []
  (let [f (fn [xs] (zero? (first xs)))
        z (every-pred f)]
    (z [0 1 0 1])))

(defn función-every-pred-3
  []
  (let [f (fn [xs] (nil? xs))
        z (every-pred f)]
    (z [])))

(defn función-every-pred-4
  []
  (let [f (fn [xs] (false? (pos? (last xs))))
        z (every-pred f)]
    (z '(123))))

(defn función-every-pred-5
  []
  (let [f (fn [x] (sorted? x))
        z (every-pred f)]
    (z {:a 1 :b 2})))

(defn función-every-pred-6
  []
  (let [f (fn [xs] (boolean? xs))
        g (fn [x] (string? x))
        z (every-pred f g)]
    (z "hello")))

(defn función-every-pred-7
  []
  (let [f (fn [xs] (boolean? xs))
        g (fn [xs] (neg? xs))
        h (fn [x] (double? x))
        z (every-pred f g h)]
    (z 1.0)))

(defn función-every-pred-8
  []
  (let [f (fn [xs] (inc xs))
        g (fn [x] (associative? x))
        z (every-pred f g)]
    (z 13)))

(defn función-every-pred-9
  []
  (let [f (fn [x] (empty? (count x)))
        g (fn [x] (number? x))
        z (every-pred g f)]
    (z {:a 2 :b 3})))

(defn función-every-pred-10
  []
  (let [f (fn [xs] (pos? (last xs)))
        g (fn [xs] (neg? (first xs)))
        h (fn [x] (boolean? x))
        z (every-pred f g h)]
    (z [0 1 0 0 1 0 1])))

(defn función-every-pred-11
  []
  (let [f (fn [xs] (map even? xs))
        z (every-pred f)]
    (z [0 1 0 0 1 0 1])))

(defn función-every-pred-12
  []
  (let [f (fn [xs] (false? xs))
        z (every-pred f)]
    (z false)))

(defn función-every-pred-13
  []
  (let [f (fn [xs] (sorted? xs))
        g (fn [xs] (false? xs))
        h (fn [xs] (boolean? xs))
        z (every-pred f g h)]
    (z {:a 1 :b 2})))

(defn función-every-pred-14
  []
  (let [f (fn [xs] (apply < xs))
        z (every-pred f)]
    (z '(1 2 3 4 5))))

(defn función-every-pred-15
  []
  (let [f (fn [xs] (keep even? xs))
        z (every-pred f)]
    (z (range 1 10))))

(defn función-every-pred-16
  []
  (let [f (fn [xs] (map-indexed list xs))
        z (every-pred f)]
    (z  [:a :b :c])))

(defn función-every-pred-17
  []
  (let [f (fn [xs] (char? (key (first xs))))
        z (every-pred f)]
    (z {:a 1 :b 2 :c 3})))

(defn función-every-pred-18
  []
  (let [f (fn [xs] (true? (some? xs)))
        g (fn [x] (true? x))
        z (every-pred f g)]
    (z false)))

(defn función-every-pred-19
  []
  (let [f (fn [xs] (nil? (int? xs)))
        g (fn [x] (boolean? x))
        z (every-pred f g)]
    (z 13)))

(defn función-every-pred-20
  []
  (let [f (fn [xs] (vector? (list xs)))
        g (fn [x] (true? x))
        h (fn [xs] (map even? xs))
        z (every-pred f g h)]
    (z [0 1 0 0 1 0 1])))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn [xs] (remove pos? xs))
        z (fnil f nil)]
    (z [0 0 1 -1 1 2 -2 -4])))

(defn función-fnil-2
  []
  (let [f (fn [xs x] (map - xs (vector x)))
        z (fnil f nil)]
    (z [-1 -2 -3 -4] 5)))

(defn función-fnil-3
  []
  (let [f (fn [xs x ys] (> xs x ys))
        z (fnil f nil)]
    (z 23 12 21)))

(defn función-fnil-4
  []
  (let [f (fn [xs] (= true (string? xs)))
        z (fnil f nil)]
    (z "")))

(defn función-fnil-5
  []
  (let [f (fn [xs x] (* 3 xs x))
        z (fnil f nil)]
    (z 3 3)))

(defn función-fnil-6
  []
  (let [f (fn [xs x ys] (== (* x ys) (+ x xs)))
        z (fnil f nil nil)]
    (z 13 22 10)))

(defn función-fnil-7
  []
  (let [f (fn [xs x] (> x (count xs)))
        z (fnil f nil)]
    (z {:a 2} 6)))

(defn función-fnil-8
  []
  (let [f (fn [xs x y ys] (if (> xs x) (+ ys y) (- xs ys)))
        z (fnil f nil nil nil)]
    (z 13 3 23 33)))

(defn función-fnil-9
  []
  (let [f (fn [x] (true? x))
        z (fnil f nil)]
    (z 0)))

(defn función-fnil-10
  []
  (let [f (fn [xs x] (str (true? xs) (false? x)))
        z (fnil f nil)]
    (z true true)))

(defn función-fnil-11
  []
  (let [f (fn [xs x ys y] (* xs x ys y))
        z (fnil f nil nil nil)]
    (z 3 3 3 3)))

(defn función-fnil-12
  []
  (let [f (fn [xs x ys] (== ys (+ xs x)))
        z (fnil f nil nil)]
    (z 1 2 3)))

(defn función-fnil-13
  []
  (let [f (fn [xs x] (== (last xs)(first x)))
        z (fnil f nil nil)]
    (z [1 2 3] [3 2 1])))

(defn función-fnil-14
  []
  (let [f (fn [xs x ys] (> ys (+ (first xs) (first x))))
        z (fnil f nil nil)]
    (z [1 2 3] [3 2 1] 13)))

(defn función-fnil-15
  []
  (let [f (fn [x] (string? (str x)))
        z (fnil f nil)]
    (z "Conradrc27")))

(defn función-fnil-16
  []
  (let [f (fn [xs x ys y] (/ xs (- (* (+ x ys) y) y)))
        z (fnil f nil nil nil)]
    (z 900 6 3 8)))

(defn función-fnil-17
  []
  (let [f (fn [xs x ys] (- xs x ys))
        z (fnil f nil nil nil)]
    (z 900 3 3)))

(defn función-fnil-18
  []
  (let [f (fn [xs x] (count (into xs x)))
        z (fnil f nil nil)]
    (z [900 3] [3 900])))

(defn función-fnil-19
  []
  (let [f (fn [xs] (dec xs))
        z (fnil f nil)]
    (z 900)))

(defn función-fnil-20
  []
  (let [f (fn [xs x] (if (> xs x) (- xs x)))
        z (fnil f nil nil)]
    (z 900 3)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


(defn función-juxt-1
  []
  (let [f (fn [xs] (coll? xs))
        z (juxt f)]
    (z [0 1 2 3 4 5])))

(defn función-juxt-2
  []
  (let [f (fn [xs] (map? xs))
        z (juxt f)]
    (z {:a 1 :b 2})))

(defn función-juxt-3
  []
  (let [f (fn [xs] (* 2 xs))
        g (fn [x] (- x x))
        z (juxt f g)]
    (z 13)))

(defn función-juxt-4
  []
  (let [f (fn [xs] (* xs xs))
        g (fn [x] (dec x))
        z (juxt f g)]
    (z 900)))

(defn función-juxt-5
  []
  (let [f (fn [xs] (count xs))
        z (juxt f)]
    (z [1 2 3 4 5])))

(defn función-juxt-6
  []
  (let [f (fn [xs] (first xs))
        z (juxt f)]
    (z [1 2 3 4 5])))

(defn función-juxt-7
  []
  (let [f (fn [x] (first x))
        z (juxt f)]
    (z "Anonymus")))

(defn función-juxt-8
  []
  (let [f (fn [xs] (list? xs))
        z (juxt f)]
    (z '("hola" "people"))))

(defn función-juxt-9
  []
  (let [f (fn [xs] (empty? xs))
        g (fn [xs] (vector? xs))
        h (fn [x] (boolean? x))
        z (juxt f g h)]
    (z #{})))

(defn función-juxt-10
  []
  (let [f (fn [xs] (first xs))
        g (fn [x] (number? x))
        z (juxt g f)]
    (z {:a 1 :b "2"})))

(defn función-juxt-11
  []
  (let [f (fn [xs] (sort xs))
        g (fn [x] (into #{} x))
        z (juxt f g)]
    (z [0 1 0 1 0 1])))

(defn función-juxt-12
  []
  (let [f (fn [xs] (pos? xs))
        g (fn [x] (ratio? x))
        z (juxt f g)]
    (z 13)))

(defn función-juxt-13
  []
  (let [f (fn [xs] (string? (str xs)))
        g (fn [xs] (boolean? xs))
        h (fn [x] (true? x))
        z (juxt f g h)]
    (z 3)))

(defn función-juxt-14
  []
  (let [f (fn [xs] (neg? xs))
        g (fn [x] (number? x))
        z (juxt f g)]
    (z 9000)))

(defn función-juxt-15
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [x] (boolean? x))
        z (juxt f g)]
    (z [1 2 3 4 5])))

(defn función-juxt-16
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [x] (boolean? x))
        h (fn [x] (true? x))
        z (juxt f g h)]
    (z "Hello")))

(defn función-juxt-17
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (number? xs))
        z (juxt f)]
    (z '("hello" "people" "conradrc27"))))

(defn función-juxt-18
  []
  (let [f (fn [xs] (* xs xs))
        g (fn [x] (inc x))
        z (juxt f g)]
    (z 900)))

(defn función-juxt-19
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (associative? xs))
        h (fn [x] (vector? x))
        z (juxt f g h)]
    (z [1 2 3 4 5])))

(defn función-juxt-20
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [xs] (map dec xs))
        z (juxt g f)]
    (z [1 2 3 4 5])))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

(defn función-partial-1
  []
  (let [f (fn [xs] (vals xs))
        z (partial f)]
    (z {:a 1 :b 2 :c 3 :d 4 :e 5})))

(defn función-partial-2
  []
  (let [f (fn [xs x] (if (> xs x) (+ xs xs)))
        z (partial f 3)]
    (z 13)))

(defn función-partial-3
  []
  (let [f (fn [xs x] (concat xs x))
        z (partial f)]
    (z [-1 -2 -3 -4 -5] [1 2 3 4])))

(defn función-partial-4
  []
  (let [f (fn [xs] (dec xs))
        z (partial f)]
    (z 13)))

(defn función-partial-5
  []
  (let [f (fn [xs] (inc xs))
        z (partial f)]
    (z 13)))

(defn función-partial-6
  []
  (let [f (fn [xs x] (contains? xs x))
        z (partial f [1 2 3 4 5])]
    (z 5)))

(defn función-partial-7
  []
  (let [f (fn [xs x] (+ x (* xs x)))
        z (partial f 10)]
    (z 13)))

(defn función-partial-8
  []
  (let [f (fn [xs x ys] (hash-set xs x))
        z (partial f 13)]
    (z 1 2)))

(defn función-partial-9
  []
  (let [f (fn [xs x] (conj [] xs x))
        z (partial f)]
    (z [123 123 123] [-1 -2 -3])))

(defn función-partial-10
  []
  (let [f (fn [xs] (map str xs))
        z (partial f)]
    (z '(1 2 3 4))))

(defn función-partial-11
  []
  (let [f (fn [xs x] (contains? xs x))
        z (partial f {:a 1 :b 3})]
    (z :f)))


(defn función-partial-12
  []
  (let [f (fn [xs] (+ (* xs xs) (inc xs)))
        z (partial f)]
    (z 13)))

(defn función-partial-13
  []
  (let [f (fn [xs x] (conj [1 2 3] (map str xs) (map str x)))
        z (partial f '(1 2 3 4))]
    (z '(1 2 3 4))))

(defn función-partial-14
  []
  (let [f (fn [xs ys] (map + xs ys))
        z (partial f)]
    (z [123 123 123] [-1 -2 -3])))

(defn función-partial-15
  []
  (let [f (fn [xs x ys y] (if (> xs x) (* x x) (- y ys y)))
        z (partial f 1)]
    (z 13 13 13)))

(defn función-partial-16
  []
  (let [f (fn [xs x] (into xs x))
        z (partial f [ 1 2 3])]
    (z [1 2 3 4])))

(defn función-partial-17
  []
  (let [f (fn [x] (* 9 x x))
        z (partial f)]
    (z 13)))

(defn función-partial-18
  []
  (let [f (fn [xs x ys y] (vector xs x ys y))
        z (partial f)]
    (z [1 2 3] [1 2 ] [1 2 3 4] [12 13])))

(defn función-partial-19
  []
  (let [f (fn [x] (sequential? x))
        z (partial f)]
    (z [-1 -2 -3 3 2 1])))

(defn función-partial-20
  []
  (let [f (fn [x] (filter char? x))
        z (partial f)]
    (z "Hello people")))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)


(defn función-some-fn-1
  []
  (let [f (fn [x] (symbol? x))
        z (some-fn f)]
    (z \c)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (char? x))
        z (some-fn g f)]
    (z \c)))

(defn función-some-fn-3
  []
  (let [f (fn [x] (+ x x))
        g (fn [x] (* x x))
        z (some-fn f g)]
    (z 13)))

(defn función-some-fn-4
  []
  (let [f (fn [x] (even? x))
        z (some-fn f)]
    (z 13)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (some? x))
        g (fn [x] (boolean? x))
        h (fn [x] (true? x))
        z (some-fn f g h)]
    (z false)))

(defn función-some-fn-6
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (zero? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z 13)))

(defn función-some-fn-7
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (associative? xs))
        h (fn [xs] (vector? xs))
        z (some-fn f g h)]
    (z '(1 2 3 4 23))))

(defn función-some-fn-8
  []
  (let [f (fn [xs] (map boolean? xs))
        g (fn [xs] (map even? xs))
        z (some-fn f g)]
    (z (range 10))))

(defn función-some-fn-9
  []
  (let [f (fn [xs] (number? (contains? xs 123)))
        z (some-fn f)]
    (z {:a 7 :b 4})))

(defn función-some-fn-10
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (associative? x))
        h (fn [x] (sequential? x))
        z (some-fn f g h)]
    (z '(range 20))))

(defn función-some-fn-11
  []
  (let [f (fn [xs] (associative? xs))
        g (fn [xs x] (contains? xs x))
        z (some-fn f g)]
    (z {:a 1 :b 2} :f)))

(defn función-some-fn-12
  []
  (let [f (fn [xs] (* xs xs))
        g (fn [x] (/ x x))
        h (fn [x] (- x x))
        z (some-fn f g h)]
    (z 13)))

(defn función-some-fn-13
  []
  (let [f (fn [xs] (seq? (seq xs)))
        g (fn [xs] (vector? xs))
        h (fn [xs] (boolean? xs))
        z (some-fn f g h)]
    (z (range 10))))

(defn función-some-fn-14
  []
  (let [f (fn [xs] (sorted? (sorted-set xs)))
        g (fn [xs] (vector? xs))
        g (fn [x] (true? x))
        z (some-fn f g)]
    (z [1 2 3 4])))

(defn función-some-fn-15
  []
  (let [f (fn [x] (rational? x))
        g (fn [x] (neg? x))
        h (fn [x] (boolean? x))
        z (some-fn f g h)]
    (z 3.1415)))

(defn función-some-fn-16
  []
  (let [f (fn [xs] (filter even? xs))
        z (some-fn f)]
    (z '(1 2 3 4))))

(defn función-some-fn-17
  []
  (let [f (fn [xs] (list? xs))
        z (some-fn f)]
    (z [132 223 323 123])))

(defn función-some-fn-18
  []
  (let [f (fn [x] (neg? x))
        z (some-fn f)]
    (z -13)))

(defn función-some-fn-19
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (boolean? x))
        h (fn [x] (true? x))
        z (some-fn f g h)]
    (z "Anonymus")))

(defn función-some-fn-20
  []
  (let [f (fn [xs] (string? (contains? xs "Uno")))
        z (some-fn f)]
    (z {:a "Uno" :b "dos" :c "(range 13)"})))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)